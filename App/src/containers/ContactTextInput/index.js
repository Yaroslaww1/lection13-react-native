import React, { useState } from 'react';
import {
  View,
  TextInput,
  StyleSheet
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const ContactTextInput = ({
  onSearchSubmit
}) => {
  const [value, onChangeText] = useState('');

  return (
    <View style={styles.searchSection}>
      <Icon
        style={styles.searchIcon}
        name="ios-search"
        size={20}
        color="#000"
      />
      <TextInput
          style={styles.input}
          placeholder="Search"
          value={value}
          onChangeText={text => onChangeText(text)}
          onSubmitEditing={() => onSearchSubmit(value)}
      />
    </View>
  );
};

const styles = StyleSheet.create({
  searchSection: {
    margin: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 20,
    borderColor: '#333',
    borderWidth: 2,
    borderRadius: 10
  },
  searchIcon: {
    padding: 10,
  },
  input: {
    flex: 1,
    paddingTop: 10,
    paddingRight: 10,
    paddingBottom: 10,
    paddingLeft: 0,
    color: '#333',
  },
});

export default ContactTextInput;
