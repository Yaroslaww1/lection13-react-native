import React from 'react';
import {
  SafeAreaView,
  FlatList,
  StyleSheet,
  StatusBar,
  Text
} from 'react-native';
import ContactView from './ContactItemView';

const ContactsListView = ({
  contacts,
  onContactClick
}) => {
  const renderItem = ({ item }) => (
    <ContactView
      onClick={() => onContactClick(item)}
      contact={item}
    />
  );

  return (
    <SafeAreaView style={styles.container}>
      { 
        contacts.length 
        ? <FlatList
            data={contacts}
            renderItem={renderItem}
            keyExtractor={item => item.recordID}
          />
        : <Text style={styles.text}>No contacts was found</Text>
      }
    </SafeAreaView>
  );
};

const styles = StyleSheet.create({
  container: {
    marginTop: StatusBar.currentHeight || 0
  },
  text: {
    textAlign: 'center'
  }
});

export default ContactsListView;
