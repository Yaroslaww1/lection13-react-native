import React from 'react';
import {
  TouchableHighlight,
  View,
  Text,
  StyleSheet
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const ContactView = ({
  contact,
  onClick
}) => {
  const { phoneNumbers } = contact;
  const phoneNumber = phoneNumbers.length ? phoneNumbers[0].number : 'No phone number set';
  return (
    <TouchableHighlight
      onPress={onClick}
      underlayColor="white"
    >
      <View
        style={styles.item}
        on
      >
        <Icon
          style={styles.icon}
          name="ios-person-circle-outline"
          size={20}
          color="#000"
        />
        <View style={styles.body}>
          <Text>{contact.givenName}</Text>
          <Text>{phoneNumber}</Text>
        </View>
        <Icon
          name="ios-information-circle-outline"
          size={20}
          color="#000"
        />
      </View>
    </TouchableHighlight>
  );
};

const styles = StyleSheet.create({
  item: {
    backgroundColor: '#cad1e8',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  icon: {
    fontSize: 36,
    flex: 1
  },
  body: {
    flexDirection: 'column',
    flex: 2.5
  }
});

export default ContactView;
