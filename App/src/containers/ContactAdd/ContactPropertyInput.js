import React from 'react';
import { 
  View,
  Text,
  StyleSheet,
  TextInput
} from 'react-native';

const ContactPropertyView = ({
  header,
  value,
  onChangeText,
  error
}) => {
  return (
    <View style={styles.wrapper}>
      <Text style={styles.header}>{header}</Text>
      <TextInput
        style={[styles.body, error ? styles.error : null]}
        value={value}
        onChangeText={onChangeText}
      />
      {
        error ? <Text style={{color: 'red'}}>{error}</Text> : null
      }
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    justifyContent: 'space-between'
  },
  header: {
    flex: 2,
    fontSize: 12,
    opacity: 0.55,
    textAlignVertical: 'bottom'
  },
  body: {
    flex: 1,
    fontSize: 18,
    borderBottomWidth: 2,
    borderBottomColor: '#b1b3ba',
    textAlignVertical: 'bottom',
    color: '#333'
  },
  error: {
    color: 'red',
    borderBottomColor: 'red',
  }
});

export default ContactPropertyView;
