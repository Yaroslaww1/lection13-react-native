import React, {useState, useEffect} from 'react';
import {
  Text,
  View,
  StyleSheet
} from 'react-native';
import {PermissionsAndroid} from 'react-native';
import Contacts from 'react-native-contacts';
import { useIsFocused } from "@react-navigation/native";
import ContactsListView from '../../views/ContactsListView';
import ContactTextInput from '../ContactTextInput';
import { TouchableOpacity } from 'react-native-gesture-handler';

const ContactsList = ({
  navigation,
  route
}) => {
  const [filters, setFilters] = useState([{}]);
  const [contacts, setContacts] = useState([]);
  const isFocused = useIsFocused();

  const filter = array => {
    return array.filter(item => {
      for (const filter of filters) {
        if (item[filter.key] !== filter.value)
          return false;
        return true;
      }
    })
  }

  useEffect(() => {
    const getPermission = async () => {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.READ_CONTACTS,
        {
          title: 'Contacts',
          message: 'This app would like to view your contacts.',
          buttonPositive: 'Please accept bare mortal',
        },
      );
  
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        Contacts.getAll((err, allContacts) => {
          if (err === 'denied') {
          } else {
            console.log('Contacts set');
            setContacts(filter(allContacts));
          }
        });
      }
    } 

    try {
      getPermission();
    } catch (err) {
      console.error(err);
    }
  }, [filters, isFocused]);

  const onSearchSubmit = text => {
    if (text === '') 
      return setFilters([{}]);
    setFilters([{ key: 'givenName', value: text }]);
  }

  const onContactClick = contact => {
    navigation.navigate('ContactInfo', { contact });
  }

  const onAddContactClick = () => {
    navigation.navigate('ContactAdd');
  }

  return (
    <View style={styles.wrapper}>
      <ContactTextInput 
        onSearchSubmit={onSearchSubmit}
      />
      <ContactsListView
        contacts={contacts}
        onContactClick={onContactClick}
      />
      <View
        style={styles.buttonWrapper}
      >
        <TouchableOpacity
          style={styles.button}
          onPress={onAddContactClick}
        >
          <Text style={styles.text}>+</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flexDirection: 'column',
    flex: 1
  },  
  button: {
    borderRadius: 70,
    backgroundColor: '#cad1e8',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  text: {
    fontSize: 54,
    marginBottom: 4,
    color: '#333'
  },
  buttonWrapper: {
    height: 75,
    width: 75,
    alignSelf: 'flex-end',
    flex: 1,
    justifyContent: 'flex-end',
    marginBottom: 26,
    marginRight: 26,
  }
});

export default ContactsList;
