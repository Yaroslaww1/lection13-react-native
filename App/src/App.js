import React from 'react';
import ContactsList from './containers/ContactsList';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';

import ContactInfo from './containers/ContactInfo';
import ContactAdd from './containers/ContactAdd';

const Stack = createStackNavigator();

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="ContactsList">
        <Stack.Screen
          name="ContactsList"
          component={ContactsList}
          options={{
            headerShown: false
          }}
        />
        <Stack.Screen
          name="ContactInfo"
          component={ContactInfo}
          options={{
            headerStyle: {
              backgroundColor: '#fff'
            },
            headerTransparent: true,
            headerTintColor: '#333',
            headerTitle: ''
          }}
        />
        <Stack.Screen
          name="ContactAdd"
          component={ContactAdd}
          options={{
            headerStyle: {
              backgroundColor: '#fff'
            },
            headerTransparent: true,
            headerTintColor: '#333',
            headerTitle: ''
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
export default App;
