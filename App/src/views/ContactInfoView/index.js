import React, { useState } from 'react';
import { 
  View,
  Text,
  StyleSheet,
  Image
} from 'react-native';
import ContactPropertyView from './ContactPropertyView';
import ActionsView from './ActionsView';

const ContactInfoView = ({
  name,
  phoneNumber,
  avatarSrc,
  onCall,
  onDelete
}) => {
  return (
    <View style={styles.wrapper}>
      <Image
        style={styles.avatar}
        source={avatarSrc}
      />
      <View style={styles.info}>
        <ContactPropertyView
          header='Name'
          body={name}
        />
        <ContactPropertyView
          header='Phone number'
          body={phoneNumber}
        />
      </View>
      <View style={styles.actions}>
        <ActionsView
          onCall={onCall}
          onDelete={onDelete}
        />
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    padding: 20,
    marginTop: 55,
    flex: 1,
  },
  avatar: {
    flex: 2,
    width: 150,
    height: 150,
    alignSelf: 'center',
    resizeMode: 'contain'
  },
  info: {
    flex: 2,
  },
  actions: {
    flex: 3.5
  }
});

export default ContactInfoView;
