import React, { useState } from 'react';
import { 
  View,
  Text,
  StyleSheet,
  Image,
  TouchableHighlight,
  KeyboardAvoidingView
} from 'react-native';
import ContactPropertyInput from './ContactPropertyInput';
import { CommonActions } from '@react-navigation/native';
import Contacts from 'react-native-contacts';

const ContactAdd = ({
  navigation,
  route
}) => {
  const [contact, setContact] = useState({ phoneNumber: '', givenName: '' });
  const [errors, setErrors] = useState({ phoneNumber: 'Phone number can\'t be empty', givenName: 'Contact`s name can\'t be empty' });

  const onAdd = () => {
    if (errors.givenName !== '' || errors.phoneNumber !== '')
      return;
    const contactToAdd = {
      givenName: contact.givenName,
      phoneNumbers: [{
        label: 'phone',
        number: contact.phoneNumber
      }]
    }
    Contacts.addContact(contactToAdd, (err) => {
      if (err) {
        console.error(err)
      }
      navigation.dispatch(CommonActions.goBack());
    })
  }

  const toggleError = (name, newValue) => {
    setErrors({
      ...errors,
      [name]: newValue ? '' : `${name} can't be empty`
    });
  }

  const onChange = (name, newValue) => {
    setContact({
      ...contact,
      [name]: newValue
    });
    toggleError(name, newValue);
  }

  return (
    <KeyboardAvoidingView
      behavior={Platform.OS == "ios" ? "padding" : "height"}
      keyboardVerticalOffset={Platform.OS == "ios" ? 0 : 20}
      enabled={Platform.OS === "ios" ? true : false}
      style={styles.wrapper}
    >
      <View style={styles.info}>
        <ContactPropertyInput
          header='Name'
          value={contact.givenName}
          onChangeText={(val) => onChange('givenName', val)}
          error={errors.givenName}
        />
        <ContactPropertyInput
          header='Phone number'
          value={contact.phoneNumber}
          onChangeText={(val) => onChange('phoneNumber', val)}
          error={errors.phoneNumber}
        />
      </View>
      <View style={styles.actions}>
        <TouchableHighlight
          style={styles.button}
          onPress={onAdd}>
          <Text>Add</Text>
        </TouchableHighlight>
      </View>
    </KeyboardAvoidingView>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    padding: 20,
    marginTop: 55,
    flex: 1,
  },
  avatar: {
    flex: 2,
    width: 150,
    height: 150,
    alignSelf: 'center',
    resizeMode: 'contain'
  },
  info: {
    flex: 2,
  },
  actions: {
    flex: 3.5,
    padding: 25,
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  button: {
    padding: 10,
    borderRadius: 20,
    width: 120,
    height: 70,
    backgroundColor: '#cad1e8',
    justifyContent: 'center',
    alignItems: 'center'
  }
});

export default ContactAdd;
