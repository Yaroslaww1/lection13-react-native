import React, { useState } from 'react';
import {
  View,
  TouchableHighlight,
  StyleSheet,
  Text
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';

const ActionsView = ({
  onCall,
  onDelete
}) => {
  return (
    <View style={styles.wrapper}>
      <TouchableHighlight onPress={onCall}>
        <View style={styles.buttonLeft}>
          <Icon
            style={styles.iconLeft}
            name="ios-call-outline"
            size={20}
            color="#000"
          />
          <Text style={styles.textLeft}>Call</Text>
        </View>
      </TouchableHighlight>
      <TouchableHighlight onPress={onDelete}>
        <View style={styles.buttonRight}>
          <Icon
            style={styles.iconRight}
            name="ios-close-outline"
            size={20}
            color="#000"
          />
          <Text style={styles.textRight}>Delete</Text>
        </View>
      </TouchableHighlight>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    padding: 25,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  buttonLeft: {
    backgroundColor: '#cad1e8',
    padding: 10,
    borderRadius: 20,
    width: 120,
    flexDirection: 'row'
  },
  iconLeft: {
    flex: 4
  },
  textLeft: {
    flex: 5,
    fontSize: 16
  },
  buttonRight: {
    backgroundColor: '#cad1e8',
    padding: 10,
    borderRadius: 20,
    width: 120,
    flexDirection: 'row-reverse'
  },
  iconRight: {
    flex: 4
  },
  textRight: {
    textAlign: 'center',
    flex: 15,
    fontSize: 16
  }
});

export default ActionsView;
