import React from 'react';
import { 
  View,
  Text,
  StyleSheet
} from 'react-native';

const ContactPropertyView = ({
  header,
  body
}) => {

  return (
    <View style={styles.wrapper}>
      <Text style={styles.header}>{header}</Text>
      <Text style={styles.body}>{body}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  wrapper: {
    flex: 1,
    justifyContent: 'space-between'
  },
  header: {
    flex: 2,
    fontSize: 12,
    opacity: 0.55,
    textAlignVertical: 'bottom'
  },
  body: {
    flex: 1,
    fontSize: 18,
    borderBottomWidth: 2,
    borderBottomColor: '#b1b3ba',
    textAlignVertical: 'bottom'
  }
});

export default ContactPropertyView;
