import React, { useState } from 'react';
import RNImmediatePhoneCall from 'react-native-immediate-phone-call';
import { CommonActions } from '@react-navigation/native';
import ContactInfoView from '../../views/ContactInfoView'
import Contacts from 'react-native-contacts';

const ContactInfo = ({
  navigation,
  route
}) => {
  const { contact } = route.params;
  const { givenName: name, phoneNumbers } = contact;
  const avatarSrc = contact.hasThumbnail ? { uri: contact.thumbnailPath } : require('../../res/img/profile-picture.png');
  const phoneNumber = phoneNumbers.length ? phoneNumbers[0].number : 'No phone number set';
  
  const onCall = () => {
    if (phoneNumber) {
      RNImmediatePhoneCall.immediatePhoneCall(phoneNumber);
    }
  }

  const onDelete = () => {
    Contacts.deleteContact(contact, (err, recordId) => {
      if (err) {
        console.error(err)
      }
      navigation.dispatch(CommonActions.goBack());
    })
  }

  return (
    <ContactInfoView
      name={name}
      phoneNumber={phoneNumber}
      avatarSrc={avatarSrc}
      onCall={onCall}
      onDelete={onDelete}
    />
  );
};

export default ContactInfo;
